# Isograd

## Contenu du projet

Isograd organise pour le compte de ses clients (Job boards, cabinets de recrutement, grands groupes, organisateurs d'événements) des Battle Dev permettant à plusieurs milliers de candidats de s'affronter simultanément.  
Le projet contient mes réponses à certains des exercices disponibles [sur le site d'Isograd](https://www.isograd.com/FR/solutionconcours.php).

* 2018-03-BDRJ ([Battle Dev RegionsJob - Mars 2018](https://www.isograd.com/FR/solutionconcours.php?contest_id=31&que_str_id=&reg_typ_id=2) // 3 exercices/6)
* 2018-11-BDHW ([Battle Dev Hello Work - Novembre 2018](https://www.isograd.com/FR/solutionconcours.php?contest_id=44&que_str_id=&reg_typ_id=2) // 4 exercices/6)
* 2019-DevChampion ([Dev Champion - 2019](https://www.isograd.com/FR/solutionconcours.php?contest_id=59&que_str_id=&reg_typ_id=2) // 5 exercices/6)