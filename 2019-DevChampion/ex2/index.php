<?php
$examples = range(1, 7);
$inputs = array();
$outputs = array();

foreach ($examples as $example) {
    $file = fopen("./input$example.txt", "r");
    while (! feof($file))
        $inputs[$example][] = trim(fgets($file));
    fclose($file);

    $file = fopen("./output$example.txt", "r");
    while (! feof($file))
        $outputs[$example][] = trim(fgets($file));
    fclose($file);
}

foreach ($inputs as $example => $input) {
    echo "<h1 style='color: black; font-size: 1.5em; font-weight: normal;'>Exemple $example</h1>";

    // <process>
    $nb_boats = intval(array_shift($input));
    $result = 0;
    foreach (array_map("intval", $input) as $nb_passengers) {
        $result += intval($nb_passengers / 10);
        if ($nb_passengers % 10 != 0)
            $result ++;
    }

    echo $result;
    // </process>

    $wanted = "";
    $returned = "";
    $ok = false;
    if (count($outputs[$example]) == 1)
        $wanted = trim($outputs[$example][0]);
    else {
        foreach ($outputs[$example] as $output)
            $wanted .= $output . "\n";
        $wanted = trim($wanted);
    }
    if (is_array($result)) {
        foreach ($result as $res)
            $returned .= $res . "\n\n";
        $returned = trim($returned);
    } else
        $returned = $result;
    if (is_array($result))
        $ok = in_array($wanted, $result);
    else
        $ok = $result == $wanted;
    ?>
<table style="border-collapse: collapse; margin-top: 1%;">
	<thead>
		<tr>
			<th
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;">Valeur
				attendue</th>
			<th
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;">Valeur(s)
				retourn&eacute;e(s)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;"><?php echo str_replace("\n", "<br />", $wanted);?></td>
			<td
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;"><?php echo str_replace("\n", "<br />", $returned);?></td>
		</tr>
	</tbody>
</table>
<?php
    if ($ok) {
        ?>
<p style='color: darkgreen; font-weight: bold;'>Bonne r&eacute;ponse !</p>
<?php
    } else {
        ?><p style='color: darkred; font-weight: bold;'>R&eacute;ponse
	erron&eacute;e.</p>
<?php }?>
<hr />
<?php
}
?>
