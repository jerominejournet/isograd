<?php
$examples = range(1, 5);
$inputs = array();
$outputs = array();

foreach ($examples as $example) {
    $file = fopen("./input$example.txt", "r");
    while (! feof($file))
        $inputs[$example][] = trim(fgets($file));
    fclose($file);

    $file = fopen("./output$example.txt", "r");
    while (! feof($file))
        $outputs[$example][] = trim(fgets($file));
    fclose($file);
}

foreach ($inputs as $example => $input) {
    echo "<h1 style='color: black; font-size: 1.5em; font-weight: normal;'>Exemple $example</h1>";

    // <process>
    if (! class_exists("Friend")) {

        class Friend
        {

            public $Rates = array();

            public $Sixth = 0;

            public $Distance = 0;

            public function __construct(string $friend_rates, array $my_rates)
            {
                $this->Rates = array_map("intval", explode(" ", $friend_rates));
                $this->Sixth = array_pop($this->Rates);
                foreach ($my_rates as $index => $my_rate)
                    $this->Distance += abs($this->Rates[$index] - $my_rate);
            }
        }
    }
    $my_rates = array_map("intval", explode(" ", array_shift($input)));
    array_shift($input);
    $nb_best = intval(array_shift($input));
    $friends = array_map(function (string $friend_rates) use ($my_rates) {
        return new Friend($friend_rates, $my_rates);
    }, $input);
    uasort($friends, function (Friend $first, Friend $second) {
        if ($second->Distance < $first->Distance)
            return 1;
        return - 1;
    });
    $result = floor(array_sum(array_map(function (Friend $friend) {
        return $friend->Sixth;
    }, array_slice($friends, 0, $nb_best))) / $nb_best);
    echo $result;
    // </process>

    $wanted = "";
    $returned = "";
    $ok = false;
    if (count($outputs[$example]) == 1)
        $wanted = trim($outputs[$example][0]);
    else {
        foreach ($outputs[$example] as $output)
            $wanted .= $output . "\n";
        $wanted = trim($wanted);
    }
    if (is_array($result)) {
        foreach ($result as $res)
            $returned .= $res . "\n\n";
        $returned = trim($returned);
    } else
        $returned = $result;
    if (is_array($result))
        $ok = in_array($wanted, $result);
    else
        $ok = $result == $wanted;
    ?>
<table style="border-collapse: collapse; margin-top: 1%;">
	<thead>
		<tr>
			<th
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;">Valeur
				attendue</th>
			<th
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;">Valeur(s)
				retourn&eacute;e(s)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;"><?php echo str_replace("\n", "<br />", $wanted);?></td>
			<td
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;"><?php echo str_replace("\n", "<br />", $returned);?></td>
		</tr>
	</tbody>
</table>
<?php
    if ($ok) {
        ?>
<p style='color: darkgreen; font-weight: bold;'>Bonne r&eacute;ponse !</p>
<?php
    } else {
        ?><p style='color: darkred; font-weight: bold;'>R&eacute;ponse
	erron&eacute;e.</p>
<?php }?>
<hr />
<?php
}
?>
