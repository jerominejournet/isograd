<?php
$examples = range(1, 5);
$inputs = array();
$outputs = array();

foreach ($examples as $example) {
    $file = fopen("./input$example.txt", "r");
    while (! feof($file))
        $inputs[$example][] = trim(fgets($file));
    fclose($file);

    $file = fopen("./output$example.txt", "r");
    while (! feof($file))
        $outputs[$example][] = trim(fgets($file));
    fclose($file);
}

foreach ($inputs as $example => $input) {
    echo "<h1 style='color: black; font-size: 1.5em; font-weight: normal;'>Exemple $example</h1>";

    // <process>
    if (! function_exists("GetAnotherOrder")) {

        // Source : https://docstore.mik.ua/orelly/webprog/pcook/ch04_26.htm#phpckbk-CHP-4-EX-7
        function GetAnotherOrder($order)
        {
            $bottom = count($order) - 2;
            $top = count($order) - 1;

            while (0 <= $bottom && $order[$bottom + 1] <= $order[$bottom])
                $bottom --;

            if ($bottom == - 1)
                return null;

            while (0 <= $top && $order[$top] <= $order[$bottom])
                $top --;

            $temp = $order[$bottom];
            $order[$bottom] = $order[$top];
            $order[$top] = $temp;

            $bottom ++;
            $top = count($order) - 1;
            while ($bottom < $top) {
                $temp = $order[$bottom];
                $order[$bottom] = $order[$top];
                $order[$top] = $temp;

                $bottom ++;
                $top --;
            }

            return $order;
        }
    }
    if (! function_exists("CheckSchedule")) {

        // Retourne le nombre maximal de cr�neaux possibles
        function CheckSchedule($schedules, $student, $takens)
        {
            $choices = array_keys($schedules[$student]);

            foreach ($schedules[$student] as $choice => $time)
                foreach ($takens as $taken)
                    if (! ($taken + 60 < $time || $time + 60 < $taken))
                        if (($key = array_search($choice, $choices)) !== false)
                            unset($choices[$key]);

            $result = [
                count($takens)
            ];

            if (count($choices) == 0 && $student + 1 < count($schedules))
                $result[] = CheckSchedule($schedules, $student + 1, $takens);

            foreach ($choices as $choice) {
                $temp = $takens;
                $temp[] = $schedules[$student][$choice];
                if ($student + 1 < count($schedules))
                    $result[] = CheckSchedule($schedules, $student + 1, $temp);
                else
                    $result[] = count($temp);
            }

            return max($result);
        }
    }
    // R�cup�ration des donn�es + cr�ation d'un tableau avec tous les cr�neaux
    $nb_students = intval(array_shift($input));
    $schedules = array_map(function ($times) {
        return array_map("intval", explode(" ", $times));
    }, $input);

    // Cr�ation d'un tableau contenant les tableaux des cr�neaux dans tous les ordres possibles
    $indexes = range(0, $nb_students - 1);
    $cpt = 0;
    $orders = array();
    while ($indexes != null) {
        foreach ($indexes as $index)
            $orders[$cpt][] = $schedules[$index];
        $cpt ++;
        $indexes = GetAnotherOrder($indexes);
    }

    // R�cup�ration du maximum de cr�neaux possibles
    $result = 0;
    foreach ($orders as $order)
        $result = max($result, CheckSchedule($order, 0, []));

    echo $result;
    // </process>

    $wanted = "";
    $returned = "";
    $ok = false;
    if (count($outputs[$example]) == 1)
        $wanted = trim($outputs[$example][0]);
    else {
        foreach ($outputs[$example] as $output)
            $wanted .= $output . "\n";
        $wanted = trim($wanted);
    }
    if (is_array($result)) {
        foreach ($result as $res)
            $returned .= $res . "\n\n";
        $returned = trim($returned);
    } else
        $returned = $result;
    if (is_array($result))
        $ok = in_array($wanted, $result);
    else
        $ok = $result == $wanted;
    ?>
<table style="border-collapse: collapse; margin-top: 1%;">
	<thead>
		<tr>
			<th
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;">Valeur
				attendue</th>
			<th
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;">Valeur(s)
				retourn&eacute;e(s)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;"><?php echo str_replace("\n", "<br />", $wanted);?></td>
			<td
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;"><?php echo str_replace("\n", "<br />", $returned);?></td>
		</tr>
	</tbody>
</table>
<?php
    if ($ok) {
        ?>
<p style='color: darkgreen; font-weight: bold;'>Bonne r&eacute;ponse !</p>
<?php
    } else {
        ?><p style='color: darkred; font-weight: bold;'>R&eacute;ponse
	erron&eacute;e.</p>
<?php }?>
<hr />
<?php
}
?>
