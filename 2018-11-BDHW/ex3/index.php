<?php
$examples = range(1, 5);
$inputs = array();
$outputs = array();

foreach ($examples as $example) {
    $file = fopen("./input$example.txt", "r");
    while (! feof($file))
        $inputs[$example][] = trim(fgets($file));
    fclose($file);

    $file = fopen("./output$example.txt", "r");
    while (! feof($file))
        $outputs[$example][] = trim(fgets($file));
    fclose($file);
}

foreach ($inputs as $example => $input) {
    echo "<h1 style='color: black; font-size: 1.5em; font-weight: normal;'>Exemple $example</h1>";

    // <process>
    $N = intval(array_shift($input));
    $f = array_map("intval", explode(" ", array_shift($input)));

    $result = 0;
    $xs = range(0, $N);
    foreach ($xs as $x) {
        $y = [
            0 => 0 < $x ? $f[$x - 1] : null,
            1 => $f[$x]
        ];

        if (count(array_unique($y)) == 1 && $y[0] == $N / 2) {
            $result = "INF";
            break;
        }

        if (($y[1] == $N / 2 || 0 < $x && min($y) < $N / 2 && $N / 2 < max($y)))
            $result ++;
    }

    echo $result;
    // </process>

    $wanted = "";
    $returned = "";
    $ok = false;
    if (count($outputs[$example]) == 1)
        $wanted = trim($outputs[$example][0]);
    else {
        foreach ($outputs[$example] as $output)
            $wanted .= $output . "\n";
        $wanted = trim($wanted);
    }
    if (is_array($result)) {
        foreach ($result as $res)
            $returned .= $res . "\n\n";
        $returned = trim($returned);
    } else
        $returned = $result;
    if (is_array($result))
        $ok = in_array($wanted, $result);
    else
        $ok = $result == $wanted;
    ?>
<table style="border-collapse: collapse; margin-top: 1%;">
	<thead>
		<tr>
			<th
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;">Valeur
				attendue</th>
			<th
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;">Valeur(s)
				retourn&eacute;e(s)</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;"><?php echo str_replace("\n", "<br />", $wanted);?></td>
			<td
				style="border: solid 0.1em gray; padding: 1%; text-align: center; vertical-align: middle;"><?php echo str_replace("\n", "<br />", $returned);?></td>
		</tr>
	</tbody>
</table>
<?php
    if ($ok) {
        ?>
<p style='color: darkgreen; font-weight: bold;'>Bonne r&eacute;ponse !</p>
<?php
    } else {
        ?><p style='color: darkred; font-weight: bold;'>R&eacute;ponse
	erron&eacute;e.</p>
<?php }?>
<hr />
<?php
}
?>
